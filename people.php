<!doctype html>
<html lang="en">
   <head>
       <?php include 'components/metadata.php' ?>
        <title>Our Valuable Team | SoftValue </title>
    <meta name="title" content="Our Valuable Team | SoftValue " />
    <meta name="description" content="Our valuable team is composed of experts in software development. They used to provide quality softwares."/> 
    <meta property="og:title" content="Our Valuable Team | SoftValue " />
    <meta property="og:image" content="https://www.softvalue.life/img/softvalue-logo-og-image.jpg" />
    <meta property="og:image:type" content="image/jpg" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="600" />
    <meta property="og:url" content="https://www.softvalue.life/people" />
    <meta property="og:description" content="Our valuable team is composed of experts in software development. They used to provide quality softwares." />
   
      </head>
   <body>
        <?php include 'components/header.php' ?>

      <div class ="banner flex-container"  >
      <h1 ><?php echo $language["our-valuable-peoples"] ?> </h1>
      </div>
      </div>
      <div class = "container">
         <section id="our-valuable-founder">
            <h2>
               <?php echo $language["our-founders"] ?> 
            </h2>
            <div class="profile-card row" >
                <div class="col-md-6 col-sm-12">
                    <div class = "cards-container">
                        <img title ="BETOMBO Mariot" class="card-img-top" src="/img/softvalue-mariot.jpg" alt="Card image cap">
                        <h3>
                            <?php echo $language["founder-name-1"] ?>
                        </h3>
                        <p>
                            <?php echo $language["founder-description-1"]?>
                        </p>  
                    </div>
                </div>
                                <div class="col-md-6 col-sm-12">
                    <div class = "cards-container">
                        <img title="LOKHAT Yassin" class="card-img-top" src="/img/softvalue-yassin.jpg" alt="Card image cap">
                        <h3>
                            <?php echo $language["founder-name-2"] ?>
                        </h3>
                        <p>
                              <?php echo $language["founder-description-2"]?>
                        </p>  
                    </div>
                </div>
            </div> 

            <div class = "row linkedin-container">
                <div class="col-md-6 col-sm-12">                     
					<p>
						<?php echo $language["email"] ?> : <a title="Linkedin" href='mailto:contact@softvalue.life'>betombomariot@softvalue.life</a><br/>
						<?php echo $language["phone-number"] ?> : +23057654389<br/>
						<a title="Linkedin" href="https://www.linkedin.com/in/mariot-betombo-657758116/">
							<?php echo $language["linkedin"] ?>
						</a>   
					</p>
                </div>
                <div class=" col-md-6 col-sm-12">
					<p>
						<?php echo $language["email"] ?> :  <a title="Linkedin" href='mailto:yassinlokhat@softvalue.life'>yassinlokhat@softvalue.life</a><br/>
						<?php echo $language["phone-number"] ?> : +23058323201<br/>
						<a title="Linkedin" href="https://www.linkedin.com/in/yassin-lokhat">
							<?php echo $language["linkedin"] ?>
						</a>   
					</p>
                </div>
            </div>
         </section>
      </div>
      <div class="separator-bar container">
           
      </div>
      
      <section style="display:none" class = "our-team container">
            <h2>
                  <?php echo $language["our-people"] ?>  
            </h2>
            <div class = "gray-background" >
                <p>
                <?php echo $language["valuable-peoples"] ?>
                </p>
            </div>
         </section>
         </div>

      <div class = "container">
            <div class= "up-button-container">
            <a  href="#">
                  <img alt="go up" src="/img/up-arrow.png"/>
            </a>
            </div>
      </div>
   </body>
         <?php include 'components/footer.php' ?>

</html>