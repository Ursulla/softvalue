 
<?php
  $datetmp = "03.7-07-2019";
?>

 <script src="/js/vendor/jquery-3.3.1.min.js"></script>
   <script src="/js/vendor/bootstrap.min.js"></script>
   <script src="/js/vendor/bootstrap.bundle.min.js"></script>
   <script src="/js/main.js?date=<?php echo $datetmp ?>"></script>
   <script>
      window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
      ga('create', 'UA-137201417-1', 'auto'); ga('send', 'pageview')
   </script>


<link rel="stylesheet" href="/css/softvalue.css?date=<?php echo $datetmp ?>">
<link href="/css/bootstrap.min.css" rel="stylesheet" >
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<meta name="robots" content="INDEX, FOLLOW" />

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137201417-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137201417-1');
</script>


    

