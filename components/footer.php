<!-- FOOTER -->
<?php
 
  if(!isset($footerid)|| empty($footerid)) 
  {
      $footerid ="footer";
  }

?>
   <footer id = "<?php echo $footerid ?>">
      <div class = "container flex-container-footer">
         <p>
            <?php echo $language["copy-right"] ?>
         </p>
         <div>
         <a target="blank" href="https://www.facebook.com/SoftValue-440018996739336/?modal=admin_todo_tour">
            <img src = "/img/softvalue-fb.png" />
         </a>
         <a target="blank" href="https://www.linkedin.com/company/softvalue-life"/>
            <img src = "/img/softvalue-linkedin.png" />
         </a>
           <a  target="blank" href="https://www.youtube.com/channel/UCWMP828x0NgOHlrlD8iNKHQ">
              <img src="/img/softvalue-youtube.png">
           </a>
         </div> 
      </div>
   </footer>

  <?php include 'components/js-import.php' ?>