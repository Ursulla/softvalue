
<?php
	// include language configuration file based on selected language
	$lang = "en";
	if(isset($_GET['lang'])){ 
		$lang = $_GET['lang']; 
	} 
	require_once("components/lang/lang.".$lang.".php");
	require_once("components/function.php");

	$tabLang = array("en" => "English", "fr" => "Francais");
	$langDisplayed = strtoupper($lang);

?>
<script>
var scrollLimit = screen.height - 200;
</script>

 <nav class=" navbar navbar-expand-lg navbar-light fixed-top ">
         <a class="navbar-brand" href="<?php echo GetLink("") ?>">
         <img src="/img/softvalue-logo.2.0.png" width="115" height="115" alt="">
         </a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
         </button>
		
		<?php
			$htmltmp = "";
			foreach($tabLang as $key => $value){
				if($key == $lang)continue;

				$url = GetLanguageLink($key);
				$htmltmp = $htmltmp.'<a style="text-align:center;width:100%;display:block;" href="'.$url.'">'.$value.'</a>';
			}
		?>
     			  <a href="javascript:void(0)" id="softvalue-lang" data-content='<?php echo $htmltmp; ?>' title="<?php echo $language["chose-your-language"] ?>" data-toggle="popover" style="text-decoration:underline">
       					<?php
       					echo $langDisplayed;
       					?> </a> 	
		
         

 <div class="collapse navbar-collapse" id="navbarSupportedContent">
	<ul class="navbar-nav ml-auto">
	   <li class="nav-item active">
		  <a class="nav-link" href="<?php echo GetLink("")?>"><?php echo $language["home"] ?> <span class="sr-only"><?php echo $language["about"] ?></span></a>
	   </li>
	   <li class="nav-item" style="display: none;">
		  <a class="nav-link" href="<?php echo GetLink("about.php")?>"><?php echo $language["about"] ?> <span class="sr-only"><?php echo $language["about"] ?></span></a>
	   </li>
	   <li class="nav-item">
		  <a class="nav-link" href="<?php echo GetLink("softwares.php")?>"><?php echo $language["softwares"] ?></a>
	   </li>
	   <li class="nav-item">
		  <a class="nav-link" href="<?php echo GetLink("people.php")?>"><?php echo $language["our-people"] ?></a>
	   </li>
	   <li class="nav-item">
		  <a class="nav-link" href="<?php echo GetLink("contact.php")?>"><?php echo $language["contact"] ?></a>
	   </li>
	</ul>
	</div>
</nav>