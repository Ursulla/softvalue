<?php
function GetLink($link){
		$lang = "en";
	if(isset($_GET['lang'])){ 
		$lang = $_GET['lang']; 
	} 
	

	return  removePhpExtension((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")."://$_SERVER[HTTP_HOST]"."/".$lang."/".$link);
;
}


function GetLanguageLink($lang){

   	$string  = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$pattern = "/(\/(en|fr)\/)/i";
  	$replacement = "/".$lang."/";
   	preg_match($pattern, $string, $matches);
	if(count($matches) == 0){

		return  removePhpExtension ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")."://$_SERVER[HTTP_HOST]"."/".$lang."$_SERVER[REQUEST_URI]");
	}
   	return  removePhpExtension(preg_replace($pattern, $replacement, $string));
}

function removePhpExtension($input){
	return  str_replace(".php","",$input);

}

function GetDownloadLink($link){

	if(isset($_GET['lang'])){ 
		$lang = $_GET['lang']; 

	}else{
		$lang = "en";
	}
	
	$pattern = "/([.,\w]+)[.]([\w]+)$/i";
  	$replacement = "$1-".$lang."-".strtoupper($lang).".$2";
	preg_match($pattern, $link, $matches);
  	if(count($matches) == 0) return $link;
   	return  removePhpExtension(preg_replace($pattern, $replacement, $link));
}

function GetDownloadCount($name){
    $con = GetConnexion();
    $sql = "SELECT * FROM DOWNLOAD Where NAME ='".$name."'"; 


    $count = 0;
    if ($con == false) { 
        return $count;
    } 
      if ($res = mysqli_query($con, $sql)) { 
        if (mysqli_num_rows($res) > 0) { 
			$row = mysqli_fetch_array($res);  
			$count = $row['COUNT'];
         
        } 
        else { 
            echo "No matching records are found."; 
        } 
    } 
    else { 
        echo "ERROR: Could not able to execute $sql. "
                                    .mysqli_error($link); 
    } 
    mysqli_close($con); 
    return $count;
}

function GetConnexion(){
	 $con = @mysqli_connect("localhost", "softvqlx_softvalue", "O0_#&9Egk2E~", "softvqlx_softvalue"); 	
	 if($con == false){
		//if remote connextion is not accessible fallback to the local one
			$con = mysqli_connect("localhost", "newuser", "password", "softvalue"); 
	}
	return $con;
}

function IncrementDownloadCount($name){
	$con = GetConnexion();
    $sql = "SELECT * FROM DOWNLOAD Where NAME ='".$name."'"; 


    $count = 0;
    if ($con == false) { 
        return $count;
    } 
      if ($res = mysqli_query($con, $sql)) { 
        if (mysqli_num_rows($res) > 0) { 
			$row = mysqli_fetch_array($res);  
			$count = $row['COUNT'];
         	
			$count = $count + 1;
         	$sql = "Update DOWNLOAD set COUNT =".$count." where NAME='".$name."'";
        	$con->query($sql);
        } 
    }
    mysqli_close($con); 
    return $count;
}

$tabyoutube = array(
array("iyC2Zmuog7M","youtube", $language["slide-youtube"]),
array("CPL-v3YU1o8", "volume", $language["slide-volume"]),
array("omKa2oPdW-U", "preferences", $language["slide-preferences"]),
array("LW6cihsN9x0", "path directory", $language["slide-path-directory"]),
array("q5NqS3YE9nM", "luminosity", $language["slide-luminosity"]),
array("_mFd7fi6RrQ", "lock screen", $language["slide-lock-screen"]),
array("I_VGe9WaDOE", "historique", $language["slide-history"]),
array("FeAQxQkt4rM", "help", $language["slide-help"]),
array("L4hFc_iD48Q", "google", $language["slide-google"]),
array("kbqfise-tjM", "command creation", $language["slide-command-creation"]),
array("a7eFEeUw5z0", "command manager", $language["slide-command-manager"]),
array("lXGfXUYf2Yw", "chevron", $language["slide-chevron"])
);

function GetAllModule(){
	$con = GetConnexion();
    $sql = "Select MODULE_ID, REFERENCE,MODULE_NAME, VERSION, PRICE, ABOUT from SIGMA_MODULE;"; 
	$tab = array();
    $count = 0;
    if ($con == false) { 
        return ;
    } 
      if ($res = mysqli_query($con, $sql)) { 
        if (mysqli_num_rows($res) > 0) { 
			 while($row = $res->fetch_assoc()) {
			  $el = array("MODULE_ID" => $row["MODULE_ID"],
			   "MODULE_NAME" => $row["MODULE_NAME"],
			   "VERSION" => $row ["VERSION"],
			   "PRICE" => $row ["PRICE"],
			   "REFERENCE" => $row ["REFERENCE"],
			   "ABOUT" => $row ["ABOUT"]);
			  array_push($tab, $el);
			 }
        } 
    }

    mysqli_close($con); 
    return 	$tab;
}

function DisplaySlide($index, $tabyoutube){

	$youtube = $tabyoutube[$index - 1];
	?>
	<div class="col-3 col-md-2">
		 <img class="demo w3-opacity w3-hover-opacity-off" src="/img/slide-icone/<?php echo $youtube[0]?>.jpg" style="width:100%;cursor:pointer" onclick="currentDiv(<?php echo $index ?>)">
  		<p> <?php echo $youtube[2] ?></p>
  	</div>
  	<?php
}

