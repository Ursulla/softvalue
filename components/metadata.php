<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="Softvalue team">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="cache-control" content="no-store" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta property="og:site_name" content="SoftValue" />
<meta property="og:type" content="website" />
<meta name="google-site-verification" content="WO_NcbmWSsPD4DKSHGvdhvmTIH6mSWbJ-ZaZuE_9qK0" />


<link rel="shortcut icon" href="/img/softvalue-ico.png" class="__web-inspector-hide-shortcut__">
<!-- Bootstrap core CSS -->

    

