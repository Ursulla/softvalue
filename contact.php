<!doctype html>
<html lang="en">
   <head>
      <?php include 'components/metadata.php' ?>
          <title>Our Valuable Contact | SoftValue </title>
    <meta name="title" content="Our Valuable Contact | SoftValue" />
    <meta name="description" content="Contact us, SoftValue: society specialized in creating value with softwares."/> 
    <meta property="og:title" content="Our Valuable Contact | SoftValue " />
    <meta property="og:image" content="https://www.softvalue.life/img/softvalue-logo-og-image.jpg" />
    <meta property="og:image:type" content="image/jpg" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="600" />
    <meta property="og:url" content="https://www.softvalue.life/contact" />
    <meta property="og:description" content="Contact us, SoftValue: society specialized in creating value with softwares." />
   
 </head>
   <body>
         <?php include 'components/header.php' ?>
<script>
var scrollLimit = 0;
</script>
        <div class ="work banner flex-container"  >
            <h1 ><?php echo $language["our-contact"] ?> </h1>
        </div>
      </div>
    
      
      <div class = " contact container">
        <section id="googleMap" class ="map">
        </section>
        

        <section class="information">
            <div class = "information-group">
                <h5>
                    <?php echo $language["adress"] ?>:
                </h5>
                <p>
                    <?php echo $language["adress-value"] ?>
                </p>
            </div>
            <div class = "information-group">
                <h5>
                    <?php echo $language["phone-number"] ?>:
                </h5>
                <p>
                    <?php echo $language["phone-number-value"] ?>
                </p>
            </div>
             <div class = "information-group">
                <h5>
                    <?php echo $language["email"] ?>:
                </h5>
                <p>
                    <?php echo $language["email-value"] ?>
                </p>
            </div>

               <div class = "information-group">
                <h5>
                    <?php echo $language["you-can-find-us"] ?>:
                </h5>
               <div class = "contact-social-media-container">
                <a  target="blank" href="https://www.facebook.com/SoftValue-440018996739336/?modal=admin_todo_tour">
                    <img src="/img/softvalue-fb-contact.jpg">
                 </a>
                <a target="blank" href="https://www.linkedin.com/company/softvalue-life">
                    <img src="/img/softvalue-linkedin-contact.jpg">
                 </a>
                 <a  target="blank" href="https://www.youtube.com/channel/UCWMP828x0NgOHlrlD8iNKHQ">
                    <img src="/img/softvalue-youtube-contact.png">
                 </a>
               </div>
            </div>
        </section>
            <div class= "up-button-container">
            <a href="#">
                  <img src="/img/up-arrow.png"/>
            </a>
            </div>
      
        <p class = "relocalisation-text">
          <?php echo $language["relocalisation"]; ?>
         </br>
         <?php echo $language["loading…"];?>
        </p>
      </div>
   </body>
     <?php include 'components/footer.php' ?>

 
   <script>
function myMap() {
var mapProp= {
  center:new google.maps.LatLng(51.508742,-0.120850),
  zoom:5,
};
var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
</script>

<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDodVYc7jiHmYAqLJHAhgars2253hmLoqk&callback=myMap"></script>-->

</html>