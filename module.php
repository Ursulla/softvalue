<!doctype html>
<html lang="en">
   <head>
      <?php include 'components/metadata.php' ?>
      <title>SigmaShell | SoftValue </title>
      <meta name="title" content="Sigmashell | SoftValue" />
      <meta name="description" content="SigmaShell is a beautiful, intuitive and efficient command line. A smart shortcut. A wonderful tool to facilitate the use of programs. It accompanies you in your work through a light and easy to understand interface. "/>
      <meta property="og:title" content="SigmaShell| SoftValue " />
      <meta property="og:image" content="/img/softvalue-logo-og-image.jpg" />
      <meta property="og:image:type" content="image/jpg" />
      <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="600" />
      <meta property="og:url" content="http://www.softvalue.life/people" />
      <meta property="og:description" content="SigmaShell is a beautiful, intuitive and efficient command line. A smart shortcut. A wonderful tool to facilitate the use of programs. It accompanies you in your work through a light and easy to understand interface. ." />
   </head>
   <body id="sigmashell">
      <?php include 'components/header.php' ?>
      <script>
         var scrollLimit = 0;
      </script>
      <div class ="banner flex-container"  >
         <h1 >
            <img alt="SigmaShell" src="/img/sigmashell-logo.png" />
            <?php echo $language["sigma-shell"]?>
         </h1>
      </div>
      <div class = "terminal-background">
         <div class = "floating-gray-background">
            <div class = "container floating-background">
                
            <div><input class = "js-module-search" placholder = "search your module" /><div>
             
               <div class = "container">
                  <div class= "up-button-container">
                     <a href="#">
                     <img alt ="go up" src="/img/up-arrow.png"/>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
            
        <?php
         $footerid= "sigmashell-footer";
         include 'components/footer.php' 
         ?>

</html>