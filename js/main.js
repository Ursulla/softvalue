/* ==========================================================================
  Softvalues
     ========================================================================== */
    

    $('#up-button-container').click(function(){
      document.body.scrollTop = 0; // For Safari
      document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    });


  $('#softvalue-lang').popover({
    container: 'body',
    html : true
  })



  function downloadExtraModule(){
    alert("Extra module is still in development. Thank you :)");
  }
$('body').on('click', function (e) {
    //did not click a popover toggle or popover
    if ($(e.target).data('toggle') !== 'popover'
        && $(e.target).parents('.popover.in').length === 0) { 
        $('[data-toggle="popover"]').popover('hide');
    }
});



$(document).ready(function(){
  $(window).scroll(function(){
  	var scroll = $(window).scrollTop();
	  if (scroll >= scrollLimit) {
	    $(".navbar").css("background" , "white");
	    $(".sub-menu-item").addClass("under");
	    $(".sub-menu-item").removeClass("above");
	    
	  }

	  else{
		  $(".navbar").css("background" , "#b9b8b8"); 
		$(".sub-menu-item").addClass("above");
	    $(".sub-menu-item").removeClass("under");

	  }

	
		if(scroll >= window.innerHeight/2 - 100 ){
	    $(".sigma-sub-menu img").css("display", "inline-block");

		}else{
			$(".sigma-sub-menu img").css("display", "none");
		}
  })
})



$(document).on("click", ".js-download-sigmashell", function(){
	gasendEvent("software", "download", "sigmashell");
	incrementDownload("Sigma Shell");
});


function gasendEvent(category, action, label){
	ga('send', 'event', category, action, label)
}


/* 
 * web service url
 */
 INCREMENT_DOWNLOAD = "/ajax/increment-download";
/*
 *
 */

function incrementDownload(name){
  var url = INCREMENT_DOWNLOAD + ".php?name=" + name;
  $.ajax({url: url, success: function(result){
    	if(isNaN(result)){
    		return;
    	}
    	var oldvalue = $('.sigma-download-text').text();
		var tab = oldvalue.trim().split(" ");
		$(".sigma-download-text").text(  result + " " + tab[1]);
    	} }
   );
}



/*
*
*	Slider
*
*/

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("slides");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
  }
  x[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " w3-opacity-off";
}


function showModulPopin(module_id){
var ROOT_API_URL = "/softvalue-api/modules/get/";
var url = ROOT_API_URL + module_id;
	$.ajax({
     type: "GET",
     url: url,
     success: function(response){
     	 response = JSON.parse(response);

     	
     	$("#module-modal-view  .js-module-sigma-version").text(response.SIGMA_VERSION);
        $("#module-modal-view  .js-module-title").text(response.MODULE_NAME);
	 	$("#module-modal-view  .js-module-name").val(response.MODULE_NAME);
	 	$("#module-modal-view  .js-module-ref").val(response.REFERENCE);
	 	$("#module-modal-view  .js-module-version").val(response.VERSION);
		$("#module-modal-view  .js-module-about").text(response.ABOUT);
		$("#module-modal-view .js-module-img").attr("src","/img/module/" + response.REFERENCE + ".png");
		

		$("#second-module-modal-view  .js-module-name").text(response.MODULE_NAME);
	 	$("#second-module-modal-view  .js-module-ref").text(response.REFERENCE);
	 	$("#second-module-modal-view  .js-module-version").text(response.VERSION);
		$("#second-module-modal-view .js-module-img").attr("src","/img/module/" + response.REFERENCE + ".png");
		$("#second-module-modal-view   .js-module-sigma-version").text(response.SIGMA_VERSION);

		 /*empty first tab and then populate it
		 **/

		 if(response.PRICE != "0"){
		 	var currency = "$";

			$("#module-modal-view  .js-module-price").val(response.PRICE + " " + currency);
			$("#second-module-modal-view  .js-module-free").css("display", "none");
			$("#second-module-modal-view  .js-module-price").text(response.PRICE);

		 }else{
		 	var currency = "$";

			$("#module-modal-view  .js-module-price").val("Free");
			$("#second-module-modal-view  .js-module-free").css("display", "block");
			$("#second-module-modal-view  .js-module-price").text("Free");

		 }


		var tbody = $("#module-modal-view tbody");
		 tbody.children().each((i, e)=>{e.remove()});
 		 response[0].forEach((c, i)=>{
				tbody.append('<tr><td>'+ i + '</td><td>' + c.COMMANDNAME + '</td><td >'+ c.ISSTARTUP +'</td><td>' + c.COMMENT + '</td></tr>');
 		 });
		
		if(response[0].length == 0){
			$("#module-modal-view table").css("display", "none");
		}else{
			$("#module-modal-view table").css("display", "block");
		}

 		 $("#module-modal-view").modal("show");       
     }
});
}



function showPopinFromAppCall(signature ,module_id){
var ROOT_API_URL = "/softvalue-api/modules/get/";
var url = ROOT_API_URL + module_id;

$(".js-machin-signature").val(signature);
	$.ajax({
     type: "GET",
     url: url,
     success: function(response){
     	 response = JSON.parse(response);
     	$("#module-modal-view  .js-module-sigma-version").text(response.SIGMA_VERSION);
        $("#module-modal-view  .js-module-title").text(response.MODULE_NAME);
	 	$("#module-modal-view  .js-module-name").val(response.MODULE_NAME);
	 	$("#module-modal-view  .js-module-ref").val(response.REFERENCE);
	 	$("#module-modal-view  .js-module-version").val(response.VERSION);
		$("#module-modal-view  .js-module-about").text(response.ABOUT);
		$("#module-modal-view .js-module-img").attr("src","/img/module/" + response.REFERENCE + ".png");
		
		$("#second-module-modal-view  .js-module-name").text(response.MODULE_NAME);
	 	$("#second-module-modal-view  .js-module-ref").text(response.REFERENCE);
	 	$("#second-module-modal-view  .js-module-version").text(response.VERSION);
		$("#second-module-modal-view .js-module-img").attr("src","/img/module/" + response.REFERENCE + ".png");
		$("#second-module-modal-view   .js-module-sigma-version").text(response.SIGMA_VERSION);
 
		 /*empty first tab and then populate it
		 **/

		 if(response.PRICE != "0"){
		 	var currency = "$";

			$("#module-modal-view  .js-module-free").css("display", "none");
			$("#module-modal-view  .js-module-price").val(response.PRICE + " " + currency);
			$("#second-module-modal-view  .js-module-free").css("display", "none");
			$("#second-module-modal-view  .js-module-price").text(response.PRICE);

		 }else{
				var currency = "$";

			$("#module-modal-view  .js-module-price").val("Free");
			$("#second-module-modal-view  .js-module-free").css("display", "block");
			$("#second-module-modal-view  .js-module-price").text("Free");

		 }


		var tbody = $("#module-modal-view tbody");
		 tbody.children().each((i, e)=>{e.remove()});
 		 response[0].forEach((c, i)=>{
				tbody.append('<tr><td>'+ i + '</td><td>' + c.COMMANDNAME + '</td><td >'+ c.ISSTARTUP +'</td><td>' + c.COMMENT + '</td></tr>');
 		 });
		
		if(response[0].length == 0){
			$("#module-modal-view table").css("display", "none");
		}else{
			$("#module-modal-view table").css("display", "block");
		}

 		 $("#second-module-modal-view").modal("show");
 		 GenerateKey(signature, module_id);       
     }
});
}


function GenerateKey(signature, reference){
var ROOT_API_URL = "/softvalue-api/modules/generatekey/";
	
var sig = $(".js-machin-signature").val(); 
if (typeof signature !== 'undefined') {
	sig = signature;
}

var errormsg = "";
var error = !( /^[a-z0-9]+$/i.test(sig));


if( !/^[a-z0-9]+$/i.test(sig)){
	errormsg = "sigma signature is not correct."
}

if(/\s/i.test(sig)){
	errormsg = "signature can not contain space.";
}

if(error){
	$(".js-error-label").css("display", "block");
	$(".js-error-label").text(errormsg);
	$(".js-key-input").val("");
	$(".js-key-input").css("display", "none");
	$(".js-machin-signature").addClass("has-error");

	return;
}else{
		$(".js-button-loading").css("display", "block");
		$(".js-error-label").css("display", "none");
		$(".js-machin-signature").removeClass("has-error");
}
var ref = $("#module-modal-view .js-module-ref").val();

if (typeof reference !== 'undefined') {
	ref = reference;
}
var url = ROOT_API_URL + sig +"/" + ref ;
	$.ajax({
     type: "GET",
     url: url,
     success: function(response){
	 	$(".js-button-loading").css("display", "none");
		$(".js-key-input").css("display", "block");
		$(".js-key-input").val(response);
		 
		
		 /*empty first tab and then populate it
		 **/    
     }
});
}


/*
*	tiles sliders
*
*/


$(document).on("click","#video-view-more",function(){
	console.log("video view more");
});



function DownloadModule(){
	var ref = $("#module-modal-view .js-module-ref").val();
	var version = $("#module-modal-view .js-module-version").val();
	var name = ref + "_" + version + ".ssm";
	var url = '/download/modules/' + name;
	downloadFile(url, name);
}


$('#second-module-modal-view').on('shown.bs.modal', function() {
  	$('#module-modal-view').css('opacity', .5);
  	if(!isappcall){
  		 	$('.js-key-input').css("display", "none");
			var signature = getCookie("sigma-signature");
  		 	if( typeof signature !== "undefined"){
  		 		$('.js-machin-signature').val(signature);
  		 	}else{
  		 		  	$('.js-machin-signature').val("");
  		 	}
  	}else{
  		setCookie( "sigma-signature",appcallsignature,1)
  	 $('.js-machin-signature').val(appcallsignature);
  	}
 
});


$('#second-module-modal-view').on('hidden.bs.modal', function() {
  	$('#module-modal-view').css('opacity', 1);
});
  	



$('#module-modal-view').on('shown.bs.modal', function() {
	$("body").addClass("modal-open-2"); 
});

$('#module-modal-view').on('hidden.bs.modal', function() {
	$("body").removeClass("modal-open-2");
});




if( typeof isappcall !== 'undefined' && isappcall && typeof appcallref !== 'undefined'){

	$(document).ready(function(){
		showPopinFromAppCall(appcallsignature, appcallref);;
	});
}


function downloadFile(url, fileName, type="text/plain") {
  // Create an invisible A element
  const a = document.createElement("a");
  a.style.display = "none";
  document.body.appendChild(a);

  // Set the HREF to a Blob representation of the data to be downloaded
  a.href = url;

  // Use download attribute to set set desired file name
  a.setAttribute("download", fileName);

  // Trigger the download by simulating click
  a.click();

  // Cleanup
  window.URL.revokeObjectURL(a.href);
  document.body.removeChild(a);
}


function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}