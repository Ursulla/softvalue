<!doctype html>
<html lang="en">
   <head>
      <?php include 'components/metadata.php' ?>
      <title>Software, Value, Quality, People, Life | SoftValue </title>
      <meta name="title" content="Software, Value, Quality, People, Life | SoftValue" />
      <meta name="description" content="Bring values, improve peoples life using softwares"/>
      <meta name="keywords" content="value,software,techonology,people, life" />
      <meta property="og:title" content="Software, Value, Quality, People, Life | SoftValue" />
      <meta property="og:image" content="https://www.softvalue.life/img/softvalue-logo-og-image.jpg" />
      <meta property="og:image:type" content="image/jpg" />
      <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="600" />
      <meta property="og:url" content="http://www.softvalue.life/" />
      <meta property="og:description" content="Society specilalized in creating values through softwares." />
   </head>
   <body>
      <?php include 'components/header.php';?>
   
      <div  class =" banner flex-container"  >
          <div class = "about-us-title-container">
               <h1 ><?php echo $language["we-bring-value"] ?></h1>  
                <p>
                  <?php echo $language["we-are-softvalue"] ?>
                </p>
            </div>
      </div>
      <div class = "container">
         <section    id="how-we-do-it">
            <h2>
               <?php echo $language["how-we-do-it"] ?>
            </h2>
            <div class ="black-box-description">
               <div class="margin-container">
                         <?php echo $language["how-we-do-it-description"] ?>  :               </div>
            </div>
            <div class = "softvalue-container">
               <div id="carouselSecureControls" data-interval="false" class="carousel slide softvalue-carousel" >
                  <div class="carousel-inner">
                     <div class="carousel-item active">
                        <div class = "row">
                           <div class = "col-md-6 first-div">
                              <h4>
                                  <?php echo $language["secure-value"] ?> 
                              </h4>
                              <div class = "softvalue-icone">
                                 <img src="/img/softvalue/anti-brute-force.png" />
                              </div>
                           </div>
                           <div class = "second-div col-md-6 softvalue-text-description">
                              <div>
                                 <h5>
                                   <?php echo $language["anti-brute-force-title"] ?>
                                 </h5>
                                 <p>
                                 <?php echo $language["anti-brute-force-text"] ?>                                                       
                                 </p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="carousel-item">
                        <div class = "row">
                           <div class = "col-md-6 first-div">
                              <h4>
                              <?php echo $language["secure-value"] ?>
                              </h4>
                              <div class = "softvalue-icone">
                                 <img src="/img/softvalue/encryption.png" />
                              </div>
                           </div>
                           <div class = "second-div col-md-6 softvalue-text-description">
                              <div>
                                 <h5>
                                 <?php echo $language["encryption-title"] ?>
                                 </h5>
                                 <p>
                                 <?php echo $language["encryption-text"] ?>                                   
                                 </p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <a style="display:none" class="carousel-control carousel-control-prev" href="#carouselSecureControls" role="button" data-slide="prev">
                  <img src="/img/arrow-left.png"  alt ="arrow-left" />
                  </a>
                  <a class="carousel-control carousel-control-next" href="#carouselSecureControls" role="button" data-slide="next">
                  <img src="/img/arrow-right.png"  alt ="arrow-right" />
                  </a>
               </div>



               <div id="carouselEfficiencyControls" data-interval="false" class="carousel slide softvalue-carousel" >
                  <div class="carousel-inner">
                     <div class="carousel-item active">
                        <div class = "row">
                         <div class = "second-div col-md-6 softvalue-text-description">
                              <div>
                                 <h5>
                                 <?php echo $language["help-people-title"] ?>
                                 </h5>
                                 <p>
                                 <?php echo $language["help-people-text"] ?>                                  
                                 </p>
                              </div>
                           </div>
                           <div class = "col-md-6 first-div">
                              <h4>
                                 <?php echo $language["efficiency-value"] ?>
                              </h4>
                              <div class = "softvalue-icone">
                                 <img src="/img/softvalue/efficiency.png" />
                              </div>
                           </div>
                          
                        </div>
                     </div>
                     <div class="carousel-item">
                        <div class = "row">
                        
                           <div class = "second-div col-md-6 softvalue-text-description">
                              <div>
                                 <h5>
                                 <?php echo $language["file-merge-title"] ?>
                                 </h5>
                                 <p>
                                 <?php echo $language["file-merge-text"] ?>                                                                  
                                 </p>
                              </div>
                           </div>
                              <div class = "col-md-6 first-div">
                              <h4>
                              <?php echo $language["efficiency-value"] ?>
                              </h4>
                              <div class = "softvalue-icone">
                                 <img src="/img/softvalue/file-merge.png" />
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="carousel-item">
                        <div class = "row">
                        
                           <div class = "second-div col-md-6 softvalue-text-description">
                              <div>
                                 <h5>
                                 <?php echo $language["sbdb-title"] ?>
                                 </h5>
                                 <p>
                                 <?php echo $language["sbdb-text"] ?>                                    
                                 </p>
                              </div>
                           </div>
                            <div class = "first-div col-md-6">
                              <h4>
                                  <?php echo $language["efficiency-value"] ?>
                              </h4>
                              <div class = "softvalue-icone">
                                 <img src="/img/softvalue/sbdb.png" />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <a style="display:none" class="carousel-control carousel-control-prev" href="#carouselEfficiencyControls" role="button" data-slide="prev">
                  <img src="/img/arrow-left.png"  alt ="arrow-left" />
                  </a>
                  <a class="carousel-control carousel-control-next" href="#carouselEfficiencyControls" role="button" data-slide="next">
                  <img src="/img/arrow-right.png"  alt ="arrow-right" />
                  </a>
               </div>

                <div id="carouselTechnologyControls" data-interval="false" class="carousel slide softvalue-carousel" >
                  <div class="carousel-inner">
                     <div class="carousel-item active">
                        <div class = "row">
                           
                          
                           <div class = "first-div col-md-6">
                              <h4>
                              <?php echo $language["technology-value"] ?>
                              </h4>
                              <div class = "softvalue-icone">
                                 <img src="/img/softvalue/technology.png" />
                              </div>
                           </div>
                            <div class = "second-div col-md-6 softvalue-text-description">
                              <div> 
                                <h5>
                                <?php echo $language["best-techonology-title"] ?>
                                 </h5>
                                 <p>
                                 <?php echo $language["best-techonology-text"] ?>                                  
                                 </p>
                                
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="carousel-item">
                        <div class = "row">
                            <div class = "first-div col-md-6">
                              <h4>
                              <?php echo $language["technology-value"] ?>
                              </h4>
                              <div class = "softvalue-icone">
                                 <img src="/img/softvalue/mobile.png" />
                              </div>
                           </div>
                           <div class = "second-div col-md-6 softvalue-text-description">
                              <div>
                                 <h5>
                                 <?php echo $language["mobile-application-title"] ?>
                                 </h5>
                                 <p>
                                 <?php echo $language["mobile-application-text"] ?>                        
                                 </p>
                                 
                              </div>
                           </div>
                          
                        </div>
                     </div>
                     <div class="carousel-item">
                        <div class = "row">
                               <div class = "col-md-6 first-div">
                              <h4>
                               <?php echo $language["technology-value"] ?>
                              </h4>
                              <div class = "softvalue-icone">
                                 <img src="/img/softvalue/desktop.png" />
                              </div>
                           </div>
                           <div class = "second-div col-md-6 softvalue-text-description">
                              <div>
                                 <h5>
                                 <?php echo $language["desktop-application-title"] ?>                                    
                                 </h5>
                                 <p>
                                    <?php echo $language["desktop-application-text"] ?>
                     
                                 </p>
                              </div>
                           </div> 
                      
                        </div>
                     </div>

                     <div class="carousel-item">
                        <div class = "row">  
                           <div class = "col-md-6 first-div">
                              <h4>
                              <?php echo $language["technology-value"] ?>
                              </h4>
                              <div class = "softvalue-icone">
                                 <img src="/img/softvalue/web.png" />
                              </div>
                           </div>                        
                           <div class = "second-div col-md-6 softvalue-text-description">
                              <div>
                                 <h5>
                                 <?php echo $language["web-site-title"] ?>
                                 </h5>
                                 <p>
                                 <?php echo $language["web-site-text"] ?>
                               
                                 </p>
                              </div>
                           </div>
                        
                        </div>

                        



                     </div>
                  </div>
                  <a  style="display:none"  class="carousel-control carousel-control-prev" href="#carouselTechnologyControls" role="button" data-slide="prev">
                  <img src="/img/arrow-left.png"  alt ="arrow-left" />
                  </a>
                  <a  class="carousel-control carousel-control-next" href="#carouselTechnologyControls" role="button" data-slide="next">
                  <img src="/img/arrow-right.png"  alt ="arrow-right" />
                  </a>
               </div>

            </div>
         </section>
      </div>
      <div class = "container">
         <section class ="our-valuable-softwares">
            <h2>
               <?php echo $language["our-valuable-softwares"] ?>
            </h2>
              
          <div class ="about-softwares-tile about-sigma-section">
           <a title="Sigmashell Software" href="<?php echo GetLink("sigmashell.php")?>">
              <div class = "about-sigma-description">
                <h3>   
                <?php echo $language["sigma-shell"]; ?>
                  </h3>

                <p>
                 
                       <?php echo substr( $language["sigma-description"], 0, 180)?> ...
              
                </p>
            </div>
             <picture data-ux-state="loaded" data-ux-module="score_bootstrap/Components/Picture">
       
                  <img  src="/img/about-us-sigmashell.jpg" alt="About us sigmashell"/>
            </picture>
            
          
            </a>
         </div>


           
          
            <div class="view-more" style = "display: none;">
               <a title = "Valuable Softwares" class="view-more" href="<?php echo GetLink('softwares.php') ?>">
               <?php echo $language["view-more"] ?>
               </a>   
            </div>
         </section>
         <div class= "home up-button-container">
            <a href="#">
            <img  alt = "go up"src="/img/up-arrow.png"/>
            </a>
         </div>
      </div>
   </body>
   <?php include 'components/footer.php' ?>
</html>