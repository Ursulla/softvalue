<!doctype html>
<html lang="en">
   <head>
        <?php include 'components/metadata.php' ?>
     <title>Valuable Work | SoftValue </title>
    <meta name="title" content="Valuable Work | SoftValue " />
    <meta name="description" content="Our valuable softwares are designed to add values to peoples life. We do it by providing the highest quality of services."/> 
    <meta property="og:title" content="Valuable Work | SoftValue " />
    <meta property="og:image" content="https://www.softvalue.life/img/softvalue-logo-og-image.jpg" />
    <meta property="og:image:type" content="image/jpg" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="600" />
    <meta property="og:url" content="https://www.softvalue.life/work" />
    <meta property="og:description" content="Our valuable softwares are designed to add values to peoples life. We do it by providing the highest quality of services." />
   
  </head>
   <body>
        <?php include 'components/header.php' ?>
<script>
var scrollLimit = 0;
</script>
        <div class ="work banner flex-container"  >
            <h1 ><?php echo $language["our-valuable-softwares-2"]?></h1>
        </div>
      </div>
    
      
      <div class = "container">
         <section class ="work our-valuable-softwares">
          
            <div  class = "row">
               <div id ="valuable-softwares-1" class = "tile col-md-6 col-sm-12" >
                  <a title="Sigmashell Software" href="<?php echo GetLink("sigmashell.php")?>">
                
                  <div>
                          <div>
                              <h3> <img class="sigmashell-logo" src="/img/sigmashell-logo.png" /> <?php echo $language["sigma-shell"] ?></h3>
                              <p>
                                    <?php echo $language["improve-the-way"] ?>
                              </p>
                           </div>
                     
                  </div>
                
                </a>
               </div>
               <div id ="valuable-softwares-2" class = "tile in-progress col-md-6 col-sm-12" >
                       <a href="#"> 
                              <div>
                                    <p>
                                         <?php echo $language["in-progress"] ?>
                                    </p>
                                    <h3><img class="sigmashell-logo" src="/img/clipboard_manager.png" /> <?php echo $language["clipboard-manager-title"] ?></h3>
                                     <p>
                                        <?php echo $language["clipboard-tile-text"] ?>
                                    </p>
                              </div>
                        </a>
               </div>
            </div>
            <div class = "row">
               <div id ="valuable-softwares-3" class = "tile in-progress col-md-6 col-sm-12" >
                  <div>
                       <p>
                          <?php echo $language["in-progress"] ?>
                        </p>
                  </div>
               </div>
               <div id ="valuable-softwares-4" class = "tile in-progress col-md-6 col-sm-12" >
                  <div>
                         <p>
                             <?php echo $language["in-progress"] ?>
                        </p>
                  </div>
               </div>
            </div>

            <div class="view-more">
               <a style="display: none;" class="view-more" href="#">
                  View more
               </a>   
            </div>
         </section>
            <div class= "up-button-container">
            <a href="#">
                  <img alt="go up" src="/img/up-arrow.png"/>
            </a>
            </div>
      </div>
   </body>
     <?php include 'components/footer.php' ?>

</html>