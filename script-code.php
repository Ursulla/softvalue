<?php
function GenerateSoftValueKey($moduleRef, $simagSignature){
	$key = md5($moduleRef.$simagSignature, TRUE);
	$key = $key.md5($key, TRUE);
	$key = $key.md5($key, TRUE);
	return ToSigmaFormat($key);
}


function ToSigmaFormat($original){
	$alphabet = "abcdefghijklmnopqrstuvwiyzABCDEFGHIJKLMNOPQRSTUVWIYZ0123456789";
	
	$key = "";
	for($i = 0; $i < strlen($original); $i++){
		$c = ord($original[$i]);
		$key = $key.$alphabet[ $c % strlen($alphabet)];		
	}
	
	$key = str_replace("0", "-", $key);
	$key = str_replace("--", "-0", $key);
	if($key[0] == '-'){
		$key = substr_replace($key,"a", 0, 1);
	}
	if($key[strlen($key) - 1] == '-'){
		$key = substr_replace($key,"Z", -1);
	}

	return $key;
}

echo GenerateSoftValueKey("clipboard_manager", "eHbbVibbtbAwoMb3");

?>