<!doctype html>
<html lang="en">
   <head>
   <?php include 'components/metadata.php' ?>
    <title>About Us | SoftValue </title>
    <meta name="title" content="About Us | SoftValue " />
    <meta name="description" content="We are  SOFTVALUE, an ssii specialized in creating value  through technology."/> 
    <meta property="og:title" content="About Us | SoftValue" />
    <meta property="og:image" content="img/softvalue-logo-og-image.jpg" />
    <meta property="og:image:type" content="image/jpg" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="600" />
    <meta property="og:url" content="http://www.softvalue.life/about" />
    <meta property="og:description" content="We are  SOFTVALUE, an ssii specialized in creating value  through technology." />
 
     </head>
   <body>
         <?php include 'components/header.php' ?>

        <script>
</script>
      <div  class =" banner flex-container"  >
      <div class = "about-us-title-container">
          <h1 ><?php echo $language["about-us"] ?></h1>
          <p>
            <?php echo $language["we-are-softvalue"] ?>
          </p>
      </div>
      </div>


      </div>
    
      <div class = "container">
         <section class ="what-we-do">
            <h2>
               <?php echo $language["what-we-do"] ?>
            </h2>
          <div class ="black-box-description">
            <p> <?php echo $language["we-design-mobile-web"]?> </p>
        </div>
             
         </section>


         <section class ="about-softwares-tile about-sigma-section">
           <a title="Sigmashell Software" href="<?php echo GetLink("sigmashell.php")?>">
              <div class = "about-sigma-description">
                <h3>   
                <?php echo $language["sigma-shell"]; ?>
                  </h3>

                <p>
                 
                       <?php echo substr( $language["sigma-description"], 0, 180)?> ...
              
                </p>
            </div>
             <picture data-ux-state="loaded" data-ux-module="score_bootstrap/Components/Picture">
       
                  <img  src="/img/about-us-sigmashell.jpg" alt="About us sigmashell"/>
            </picture>
            
          
            </a>
         </section>


         <section  style="display:none;"class ="our-valuable-partners">
           <h2>
               <?php echo $language["our-loyal-partners"] ?>
           </h2>
           <div>
                  <div class ="partners-box">
                       
                  </div>
           </div>
         </section>


            <div class= "up-button-container">
            <a href="#">
                  <img src="/img/up-arrow.png"/>
            </a>
            </div>
      </div>
   </body>
      <?php include 'components/footer.php' ?>
</html>