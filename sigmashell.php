<!doctype html>
<html lang="en">
   <head>
      <?php include 'components/metadata.php' ?>
      <title>SigmaShell | SoftValue </title>
      <meta name="title" content="Sigmashell | SoftValue" />
      <meta name="description" content="SigmaShell is a beautiful, intuitive and efficient command line. A smart shortcut. A wonderful tool to facilitate the use of programs. It accompanies you in your work through a light and easy to understand interface. "/>
      <meta property="og:title" content="SigmaShell| SoftValue " />
      <meta property="og:image" content="https://www.softvalue.life/img/softvalue-logo-og-image.jpg" />
      <meta property="og:image:type" content="image/jpg" />
      <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="600" />
      <meta property="og:url" content="https://www.softvalue.life/people" />
      <meta property="og:description" content="SigmaShell is a beautiful, intuitive and efficient command line. A smart shortcut. A wonderful tool to facilitate the use of programs. It accompanies you in your work through a light and easy to understand interface. ." />
   </head>
   <body id="sigmashell">
      <?php include 'components/header.php' ?>
      <script>
         var scrollLimit = 0;
         var isappcall = false;
         <?php if(isset($_GET["mode"]) && $_GET["mode"] == "appcall"){
                //récupérer le module
                echo " var appcallsignature = '".$_GET["sig"]."';";
                  if(isset($_GET["ref"])){
                        echo " var appcallref = '".$_GET["ref"]."';";
                  }
                echo " isappcall = true;";
            }
            ?>
      </script>
      <div class ="banner flex-container"  >
         <h1 >
            <img alt="SigmaShell" src="/img/sigmashell-logo.png" />
            <?php echo $language["sigma-shell"]?>
         </h1>
      </div>
      <div class = "terminal-background">
         <div class = "floating-gray-background">
            <div class = "container floating-background">
              <nav  class ="sigma-sub-menu">
                <ul>
                  <img alt="SigmaShell" src="/img/sigmashell-logo.png">
                  <li class="item"><a href="#what-sigma-can-do">What Sigma can do</a></li>
                  <li class="item"><a href="#download">Download</a></li>
                  <li class="item"><a href="#modules">Modules</a></li>
                  <li style ="display:none;" class="item"><a href="#tutorial">Tutorial</a></li>
                </ul>
              </nav>
                <div class = "sigmashell-introduction-section">
                        <h2>
                         <?php echo $language["sigmashell-introduction"]; ?> 
                              </h2>
                  </div>
               <div class = "row">
                  <div class = "col-md-8  col-sm-12">
                
                   <div  class = "sigma-section definition">
                        <span id="what-sigma-can-do">
                        </span>
                        <h2>
                          <?php echo $language["what-sigma-can-do"]; ?> 
                        </h2>
                        <p>
                           <?php echo $language["sigma-description"]; ?>
                        </p>
                        <ul>
                           <li>
                              <?php echo $language["open-quickly-application"] ?>
                           </li>
                           <li>
                              <?php echo $language["personalize-shortcuts"] ?>
                           </li>
                           <li>
                              <?php echo $language["launche-your-command-without-cmd"] ?>
                           </li>
                           <li>
                              <?php echo $language["manage-brightness"] ?>
                           </li>   
                           <li>
                              <?php echo $language["manage-volume"] ?>
                           </li>   
                           <li>
                              <?php echo $language["configure-cmd"] ?>
                           </li>
                            <li>
                              <?php echo $language["quick-search-google"] ?>
                           </li>
                           <li>
                              <?php echo $language["launch-system-cmd"] ?>
                           </li>
                           <li>
                              <?php echo $language["lock-sceen"] ?>
                           </li>   
                           <li>
                              <?php echo $language["extending-features"] ?>
                           </li>   
                           <li>
                              <?php echo $language["get-cmd-out-put"] ?>
                           </li>
                        </ul>
                        <p>
                           </br>
                        </p>
                     </div>
               </div>
               <div class = "sigma-information-section col-md-4 col-sm-12">
                     <p class ="langages">
                        <?php echo $language["langages"] ?> : en.EN, fr.FR
                     </p>
                     <p>
                        <?php echo $language["version"] ?> : <span id='sigmashell-version'>3.1</span>
                     </p>
                   
                     <p>
                        <?php echo $language["release-date"] ?> : 26/05/2019
                     </p>
                     <p>
                        <?php echo $language["OS"] ?> : Windows     7, 8, 10
                     </p>
                     <div class="button-container">
                       <p>
                        <?php echo $language["download-count"] ?>:
                       </p>
                     </div>
                     <p class = "sigma-download-text">
                        <?php echo GetDownloadCount("Sigma Shell"); echo " ".$language["download-text"]?>
                     </p>
                     <div style = "display:none;" class ="img-container"> 
                        <a  target="blank" href="<?php echo GetDownloadLink('/download/Documentation Sigma Shell.pdf'); ?>">
                        <img alt="go up" src = "/img/documentation.png" />
                        </a>
                        <a onclick="downloadExtraModule()" href="javascript:void(0);">
                        <img alt="go up" src = "/img/plus-button.png" />
                        </a>
                     </div>
                  </div>
               </div>
                    <div class = "sigma-section">
                        <span id="download">
                        </span>
                        <h2>
                           Download:   
                        </h2>
                        <p><?php echo $language["sigma-download-setup-video"]; ?> <a  href ="/download/SigmaShell_Setup.exe" class ="js-download-sigmashell" download > Sigmashell_Setup.exe</a>.</p>
                        <p><?php echo $language["sigma-download-doc"]; ?>  <a  target="blank" href="<?php echo GetDownloadLink('/download/Documentation Sigma Shell.pdf'); ?>" ><?php echo $language["here"]?></a>.</p>
                    </div>
                
               

               <div class = " modules sigma-section">
                        <span id="modules">
                        </span>
                        <h2>
                           Modules:   
                        </h2>
                        <div class = "row">

                        <?php
                         $tabmodules = GetAllModule();
                         $count = count($tabmodules);
                         for($i = 0; $i < $count; $i++){
                            $module = $tabmodules[$i];
                              ?>
                              <div class = "module-item col-6 col-sm-3 col-md-4 col-lg-2">
                                    <img src ="/img/module/<?php echo $module["REFERENCE"]?>.png"/>
                                    <p><?php echo $module["MODULE_NAME"]?></p>
                                    <button class ="download-module-button-free" onclick="showModulPopin(<?php echo $module["MODULE_ID"]  ?>)" >Get for free </button>
                                    <button class = "download-module-button-plus" style = "display:none" onclick="showModulPopin(<?php echo $module["MODULE_ID"]  ?>)" >Get + </button>
                      
                              </div>

                              <?php
                        }?>
                            
                            
                       
                        </div>

                        <?php if ($count > 12){
                              ?> <div  class="view-more">
                           <a id="video-view-more" title = "Valuable Softwares" class="view-more" href="javascript:void(0)">
                           <?php echo $language["view-more"] ?>
                           </a> 
                                                   </div>
 
                           <?php
                        }
                        ?> 
               </div>


                  <div  style ="display:none;"  class = "sigma-section">
                        <span id="tutorial">
                        </span>
                        <h2>
                           <?php echo $language["tutorial"] ?>:   
                           </br>
                        </h2>

                         <div class ="sigmashell-video-container">
                  <div class ="video-display">
                     
                       <?php
                       $length = count($tabyoutube);
                              for($index = 0; $index < $length; $index++){
                                          $youtube = $tabyoutube[$index];
                                          $active = "";
                                          if($index == 0){
                                                $active ="video-active";
                                          }
                                        ?>
                                         <iframe class = "slides <?php echo $active ?>" width="100%" height="315"
                                                src="https://www.youtube.com/embed/<?php echo $youtube[0] ?>">
                                             </iframe>
                                        <?php  
                                    
                              }
                        ?>
                  </div>
                  <div id="video-carousel" class="carousel slide" data-ride="carousel" style ="" data-interval="false">
                     <div class = "carousel-inner">

                        <?php
                              for($index = 1; $index <= $length; $index++){
                                    $begin = $index == 1 || ($index -1) % 4 == 0;
                                    $active = "";
                                    if($index == 1){
                                          $active = "active";
                                    }


                                    if($begin){
                                         if($index != 1){
                                           ?>
                                            </div>
                                            </div>
                                        <?php  }

                                        ?>
                                            <div class ="carousel-item youtube-carousel-item <?php echo $active ?>">
                                                <div class =" video-tile ">
                                        <?php  
                                    }
                                          DisplaySlide($index, $tabyoutube);
                                    
                                      if($index == $length){
                                        ?>
                                           </div>
                                           </div>
                                        <?php  
                                    }

                              }
                        ?>
                       
                     </div>
                     <a class="carousel-control-prev" href="#video-carousel" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next" href="#video-carousel" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
                  </div>
              
                  <div style="display:none;" class="view-more">
                     <a id="video-view-more" title = "Valuable Softwares" class="view-more" href="javascript:void(0)">
                     <?php echo $language["view-more"] ?>
                     </a>   
                  </div>
               </div>
               <div class = "container">
                  <div class= "up-button-container">
                     <a href="#">
                     <img alt ="go up" src="/img/up-arrow.png"/>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>

<!-- Modal -->
<div class="modal fade"  id="module-modal-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="js-module-title" >Clipboard Manager </h5>
        <span class=" modal-free">This module is compatible with Sigma Version <span class = "js-module-sigma-version"></span> and newer versions </span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <div class = "container description-module-container">
            <img class ="js-module-img" src = ""/>
            <div class = "row">
                  <div class = "col-md-6">
                        <label  class = "" >Module Name:</label> <input class = "js-module-name" readonly value="Clipboard Manager" />
                  </div>
                  <div class ="js-container-module-price col-md-6">
                        <label class = "">Price:</label> <input readonly class = "js-module-price" value="0.5 $"/>
                  </div>
            </div>
            <div class = "row"> 
                  <div class = "col-md-6">
                        <label class = "first-column-align-label" >Module Ref:</label> <input readonly class ="js-module-ref" value="clipboardmanager" /> 
                  </div>
                  <div class = "col-md-6">
                        <label class = "second-column-align-label ">Version:</label> <input readonly class ="js-module-version" value="1.0"/>
                  </div>
            </div>
              <div class = "about-module">
                  <p class = "js-module-about">
                        This module handle clipboard of the system
                  </p>
                  <label>About the module</label>
            </div>
      </div>

            <table class="table table-dark">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Command Name</th>
                  <th scope="col">Start up</th>
                  <th scope="col">Description</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>msoffice</td>
                  <td>No</td>
                  <td>Word Editor</td>
                </tr>
          
                  <tr>
                  <th scope="row">2</th>
                  <td>msoffice</td>
                  <td>No</td>
                  <td>Word Editor</td>
                </tr>
              </tbody>
            </table>

      </div>
      <div class="modal-footer">
        <button type="button" onclick="DownloadModule();" class="btn btn-secondary" ><img src="/img/icone/download.png" /></button>
        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#second-module-modal-view"><img src="/img/icone/encryption-key.png" /></button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="second-module-modal-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="js-module-name" id="exampleModalLabel">Clipboard Manager </h5> 
                <span class=" modal-free">This module is compatible with Sigma Version <span class = "js-module-sigma-version"></span> </span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body description-module-container">
            <img class ="js-module-img" src = ""/>
       <div class = "row">
            <div class = "col-6"><label>Module Name:</label> <span class ="js-module-name">Sigmashell</span></div>
            <div class= "col-6 js-container-module-price"><label>Price:</label> <span class = "js-module-price">1.0</span></div>
      </div>
      <div class = "row">
            <div class = "col-6"><label>Ref:</label> <span class ="js-module-ref">Sigmashell</span></div>
            <div class= "col-6"><label>Version:</label> <span class = "js-module-version">1.0</span></div>
      </div>
          
      <div>
            <input class ="js-machin-signature" placeholder=  "SoftValue-Signature" name = "sigma-signature"/>
            <label class = "js-error-label"></label>
      </div>
      </div>
     <div class="modal-footer">
        <button type="button"  class="btn btn-primary" data-toggle="modal" onclick="GenerateKey()" >Generate Key</button>
        <img  style="display: none;" class = " js-button-loading" style ="display:none;" src="/img/icone/loading.gif">
        <input class="js-key-input" style = "display: none;" read-only="" value="098090878">
      </div>
    </div>
  </div>
  </div>




   </body>
            
        <?php
         $footerid= "sigmashell-footer";
         include 'components/footer.php' 
         ?>

</html>